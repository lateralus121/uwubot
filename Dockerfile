FROM python:3.7-slim

LABEL maintainer "Bucket <lateralus121@gmail.com>"

RUN apt-get update --quiet \ 
    && apt-get install \
            -y \
            --force-yes \
            --no-install-recommends \
            --no-install-suggests \
    libffi-dev \
    libnacl-dev \
    libopus0 \    
    opus-tools \ 
    ffmpeg

# Add project source

RUN mkdir /app
RUN mkdir /app/database
RUN mkdir /app/src

WORKDIR /app

ADD ./database/db.json /app/database/db.json

ADD ./requirements.txt /app/requirements.txt

RUN pip install  --no-cache-dir -r requirements.txt

ADD . /app

ENTRYPOINT ["python3", "./src/run.py"]