import discord
import re
import random
from discord.ext import commands
from discord.voice_client import VoiceClient
import asyncio
from os import listdir, environ
from os.path import isfile, join
from tinydb import TinyDB, Query
import logging

prefix_char = "?"
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
bot = commands.Bot(command_prefix=prefix_char)
faces = ['(・`ω´・)',';;w;;','owo','UwU','>w<','^w^']
filePath = 'resources/'
db = TinyDB('./database/db.json')


if not discord.opus.is_loaded():
    discord.opus.load_opus('opus')

@bot.command()
async def testMP3(ctx, *, content:str):
    result = doesFileExist(content)
    logger.info("testMP3 if file exist {0}".format(result))
    #if doesFileExist(ctx.message.content):
    channel = ctx.message.author.voice
    if channel is not None:
        logger.info("testMP3 user: {0} in channel {1}".format(ctx.message.author.name, channel.channel.name))
        vc: discord.VoiceClient = await channel.channel.connect()
        if not vc.is_playing():
            soundclip = filePath + content
            audio_source = discord.FFmpegPCMAudio(soundclip)
            logger.info('Playing sound {0}'.format(soundclip))            
            vc.play(audio_source, after=None)
            while vc.is_playing():
                await asyncio.sleep(1)
            logger.info('finish playing sound {0}'.format(soundclip))             
            vc.stop()            
            await vc.disconnect()

@bot.event
async def on_message(message):
    logger.info('on_message event {0} and if bot {1}'.format(message.author, message.author.bot))
    await bot.process_commands(message)

    if message.content.startswith(prefix_char):
        return
    if message.author == bot.user or message.author.bot is True:
        return
    logger.info('on_message event content: {0}'.format(message.content))        
    match = re.match("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$", message.content)
    emojiDetect = re.search(":.*:.*", message.content)
    logger.info('on_message event mentions: {0} - EmojiDetect {1}'.format(message.mentions, emojiDetect))
    if match is None and not message.mentions and emojiDetect is None:
        randomTime = random.randint(0,100)
        logger.info('on_message event rng chance {0}/100'.format(randomTime))
        if randomTime > 60:
            msg = re.sub("(?:r|l)", "w", message.content)
            msg = re.sub("(?:R|L)", "W", msg)
            msg = re.sub("n([aeiou])", 'ny', msg)
            msg = re.sub("N([aeiou])", 'Ny' ,msg)
            msg = re.sub("N([AEIOU])", 'Ny', msg)
            msg = re.sub("ove","uv",msg)
            msg = re.sub("\!+/"," " + random.choice(faces), msg)            
            if msg != message.content:                
                logger.info('message uwufy is not the same as original {0}=>{1}'.format(message.content, msg))
                await message.channel.send(msg)
                

@bot.event
async def on_ready():
    logger.info('Logged in as')
    logger.info(bot.user.name)
    logger.info(bot.user.id)

@bot.event
async def on_voice_state_update(member, before, after):
    if before.channel is None and after.channel is not None and member.name != "UwUBot" and member.bot is False: 
        await asyncio.sleep(2)
        vc: discord.VoiceClient = await after.channel.connect()
        if not vc.is_playing():
            soundclip = filePath + getSoundFile()
            audio_source = discord.FFmpegPCMAudio(soundclip)
            logger.info('Playing sound {0}'.format(soundclip))            
            vc.play(audio_source, after=None)
            while vc.is_playing():
                await asyncio.sleep(1)
            logger.info('finish playing sound {0}'.format(soundclip))             
            vc.stop()            
            await vc.disconnect()

@bot.command()
async def MP3List(ctx):
    files =  [f for f in listdir(filePath) if isfile(join(filePath, f))]
    files.remove(".gitignore")
    files.remove(".gitkeep")
    out = '\n'.join(files)
    embed = discord.Embed(title="MP3 List", description=out)
    await ctx.author.send(embed=embed)

@bot.command()
@commands.has_permissions(administrator=True)
async def AddGroup(ctx, *, content:str):
    logger.info('Try adding group to admin')
    for role in ctx.guild.roles:
        if role.name == content:
            db.insert({'server': ctx.guild.name, 'groups':[role.name]})
            await ctx.author.send("Group {0} from {1} added to admin group".format(role.name, ctx.guild.name))
            return
    await ctx.author.send("Cannot find {0} group on server".format(content))

@bot.command()
@commands.has_permissions(administrator=True)
async def ListGroup(ctx):
    ob = ''
    for item in db:
        ob += '{0}->'.format(item)
    await ctx.author.send('List of Objects:\n {0}'.format(ob))

@bot.command()
async def CheckIfPartOfGroup(ctx):
    Server = Query()
    serverName = ctx.guild.name
    serverId = ctx.guild.id
    logger.info('CheckIfPartOfGroup: Server Name:{0}'.format(serverName))
    result = db.search(Server.groups.any(serverName))
    logger.info('CheckIfPartOfGroup: {0}'.format(result))
    

    #if result is None:
    #    await ctx.author.send('CheckIfPartOfGroup: not found in list {0}'.format(ctx.guild.name))    
    #    return

    #for group in result:
    #    logger.info('CheckIfPartOfGroup: {0}'.format(group['groups']))



    #await ctx.author.send('CheckIfPartOfGroup: {0}'.format(result))
            
def getSoundFile():
    files =  [f for f in listdir(filePath) if isfile(join(filePath, f))]
    return random.choice(files)

def doesFileExist(filename):
    files =  [f for f in listdir(filePath) if isfile(join(filePath, f))]
    if filename in files:
        return True
    else:
        return False


bot.run(environ.get('TOKEN'))